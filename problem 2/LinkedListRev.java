class LinkedListRev{
	
	ListNode head;
	int length;
	
	static class ListNode{
		int data;
		ListNode next;
		
		ListNode(int data){
			this.data=data;
			
		}
	}
	
	boolean isEmpty(){
		return length==0;
	}
	void addlast(int data){
		ListNode Node=new ListNode(data);
		ListNode temp= head;
		if(isEmpty()){
			head=Node;
			length++;
		return;
		
		}
		while(temp.next!=null){
			temp=temp.next;
			}
			temp.next=Node;
			length++;
	}
	
	void reverse(ListNode list){
		if(list==null)
			return;
		reverse(list.next);
		System.out.print(list.data+"->");
	}
	void reverse(){
		reverse(head);
	}
	public static void main(String [] args){
		LinkedListRev s1= new LinkedListRev();
		//uncomment this to check first condition;
		//s1.addlast(1);
		//s1.addlast(2);
		//s1.addlast(3);
		//s1.addlast(4);
		//s1.addlast(5);
		//s1.reverse();
		s1.addlast(3);
		s1.addlast(4);
		s1.addlast(2);
		s1.addlast(5);
		s1.reverse();
		
	}
}